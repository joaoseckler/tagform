from django.http import HttpResponseRedirect
from django.conf import settings
from . import get_session_key, TF_ROOT, fss

def handle_uploaded_file(file):
    name = file.name

    dirname = TF_ROOT
    dirname.mkdir(exist_ok=True, parents=True)
    fname = fss.get_available_name(dirname / name)

    with open(fname, "wb+") as destination:
        for chunk in file.chunks():
            destination.write(chunk)

    return str(fname)

def view(request, name, path):
    key, file_key, redirect_key = get_session_key(name)
    files = []

    for field, file in request.FILES.items():
        fname = handle_uploaded_file(file)
        files.append((field, file.name, fname))

    if files:
        request.session[file_key] = files

    request.session[redirect_key] = True
    request.session[key] = request.POST
    return HttpResponseRedirect(f"/{path}")


