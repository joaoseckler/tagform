from uuid import uuid4
from django.core.files.uploadedfile import SimpleUploadedFile
from django.conf import settings
from pathlib import Path
from django.core.files.storage import FileSystemStorage


TF_URL = getattr(
    settings,
    "TAGFORM_URL",
    "tagform"
).lstrip("/")

TF_ROOT = Path(getattr(
    settings,
    "TAGFORM_ROOT",
    settings.MEDIA_ROOT / "tagform"
))

fss = FileSystemStorage()

def get_session_key(name):
    return (
        f"_tagform_{name}",
        f"_tagform_key_{name}",
        "tagform_redirect",
    )

class TagForm:
    def __init__(self, name, Form, process, template, takes_context):
        self.name = name
        self.Form = Form
        self.process = process
        self.template = template
        self.takes_context = takes_context

    def tag(self):
        def inner(context):
            request = context.request
            result = ""
            key, file_key, redirect_key = get_session_key(self.name)


            if key in request.session:
                files = {}
                if file_key in request.session:
                    for field, name, fname in request.session[file_key]:
                        try:
                            with open(fname, "rb") as f:
                                files[field] = SimpleUploadedFile(
                                    name,
                                    f.read()
                                )
                        except FileNotFoundError:
                            pass

                form = self.Form(request.session[key], files)
                if form.is_valid():
                    options = form.cleaned_data
                    options["session"] = request.session
                    result = self.process(options)
            else:
                form = self.Form()

            path = request.path.replace(f"{TF_URL}/", "")
            context["action"] = f"{TF_URL}/{self.name}{path}"
            context["form"] = form
            if self.takes_context:
                context.update(result)
            else:
                context["result"] = result

            if redirect_key in request.session:
                context[redirect_key] = True
                request.session.pop(redirect_key)

            return context
        return inner


def tagform_register(
    library,
    tag_name,
    Form,
    process,
    template="tagform/form.html",
    takes_context=True
):
    name = str(uuid4())[:8] + "." + tag_name

    tf = TagForm(name, Form, process, template, takes_context)
    library.inclusion_tag(
        template,
        takes_context=True,
        name=tag_name
    )(tf.tag())
