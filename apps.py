from django.apps import AppConfig


class TagformConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "tagform"
