from django.urls import re_path
from . import views

app_name = "tagform"
urlpatterns = [
    re_path(r'(?P<name>.+)/(?P<path>.+)$', views.view, name="tagform"),
]
