# Tag Forms

Create django templatetags which are forms!

## Usage

First, register `tagform` in your settings' `INSTALLED_APPS`. Add a line
to your project's `urls.py` to include this app, for example:

```python
    re_path( r'^tagform/', include("tagform.urls")),
```

Then, in your project, instead of registering a regular tag or filter,
register with tagform, passing it your form.

```python
# example/templatetags/example.py

from django import template
from tagform import tagform_register
from ..forms import ExampleForm

register = template.Library()

def process(options):
    """receives `cleaned_data` from a `is_valid()`ed form"""
    context = {
        "something": get_something(options)
    }

    # could return string or mark_safe if `takes_context` below was False
    return context

tagform_register(
    library=register,
    tag_name="example_form",
    Form=ExampeForm,
    process=process,
    # tagform will register a inclusion tag, so a template is needed.
    # The default is "tagform/form.html", a very basic form template.
    template="some/form.html",
    takes_context=True,
)
```

Then, simply use it in your template

```django
{% load example %}

<p>Here is my example form:</p>

{% example_form %}
```

## Writing the `process` function

The function will receive the `cleaned_data` from the form, after
`is_valid` has already been called on it. This dictionary will also
contain a key `session` which holds the request session. In particular,
if the page has been triggered by a redirect done by tagform, there will
be a key `tagform_redirect` in this session.

## Writing a template

The used template will receive the following variables in its context:

* `form` will hold the form object;
* `action` determines what should go in the form's `action` attribute:

  ```django
  <form action="{{ action }}" ...
  ```

* `result` will have the return value of the `process` function if the
  option `takes_context` is set to False;
* `tagform_redirect` determines if the page has been triggered by a
  redirect done by tagform. This is useful to, for example, control
  whether to download a file upon loading or not;
* If `takes_context` is set to true, `process` should return a
  dictionary, with which the context will be `dict.update`d.

## File handling

Processing of form is done after redirection to original page, as this
avoids the creation of another page/view which handles form submission
success. However, if the form contains file submissions, the file data
isn't redirected by default. Tagform saves the files in a directory
`MEDIA_ROOT / "tagform"` and read this file again after the redirection.
This isn't optimal, since we are writing a file to disk only to read it
to memory again, and it creates files which *will have to be removed
somehow* afterwards, but it guarantees that the form received by the
redirected page is the same as it would be on the page which receives
the POST data.

The name of the folder in which the temporary files are saved can be
controlled by a settings variables named `TAGFORM_ROOT`. Additionally,
`TAGFORM_URL` defines the base url of the pages controlled by tagform.

```python
# settings.py

TAGFORM_ROOT = MEDIA_ROOT / "my_custom_folder"
TAGFORM_URL = "my_custom_url"
```

If you customize `TAGFORM_URL`, do remember to update your `urls.py`:

```pytho
    re_path( r'^my_custom_url/', include("tagform.urls")),
```

